FROM node:14.16.0

RUN mkdir fullstack-frontend

WORKDIR /fullstack-frontend

ENV PATH /fullstack-frontend/node_modules/.bin:$PATH

COPY . /fullstack-frontend

RUN yarn install

EXPOSE 3000

ENTRYPOINT ["yarn","run", "dev"]
