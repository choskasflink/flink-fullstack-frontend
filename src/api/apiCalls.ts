import axios from "axios";
import {toast} from 'react-toastify';

export const getAllRegisters = async () => {
    try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_BACKEND_URL}/api/getRegisters`);
        return response.data.data;
    } catch (error) {
        toast.error(`❌ ${error.response.data.message}`);
    }
};

export const getRegisterById = async (uuid: any) => {
    try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_BACKEND_URL}/api/getRegisterByUUID?uuid=${uuid}`);
        return response.data.data;
    } catch (error) {
        toast.error(`${error.response.data.message}`);
    }
};

export const updateRegister = async (data: object) => {
    try {
        const response = await axios.put(`${process.env.NEXT_PUBLIC_BACKEND_URL}/api/updateRegister`, data);
        toast(response.data.message);
        return response.data;
    } catch (error) {
        toast.error(`${error.response.data.message}`);
        return error.response.data;
    }
};

export const deleteRegister = async (data: string) => {
    try {
        const response = await axios.delete(`${process.env.NEXT_PUBLIC_BACKEND_URL}/api/deleteRegister/${data}`);
        toast(response.data.message);
        return response.data.data;
    } catch (error) {
        toast.error(`${error.response.data.message}`);
    }
};

export const addRegister = async (data: object) => {
    try {
        const response = await axios.post(`${process.env.NEXT_PUBLIC_BACKEND_URL}/api/createRegister`, data);
        toast(response.data.message);
        return response.data;
    } catch (error) {
        toast.error(`${error.response.data.message}`);
        return error.response.data;
    }
};