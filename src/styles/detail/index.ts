import styled from "styled-components";

export const DetailWrapper = styled.div`
  background-image: linear-gradient(rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.25)),
    linear-gradient(301deg, rgba(44, 34, 150, 0), #7b2297),
    linear-gradient(rgba(0, 82, 154, 0.95), rgba(0, 82, 154, 0.95)),
    linear-gradient(121deg, rgba(109, 180, 62, 0), #6db43e),
    linear-gradient(238deg, rgba(173, 32, 142, 0), #ad208e),
    linear-gradient(
      42deg,
      rgba(0, 190, 213, 0),
      rgba(0, 190, 213, 0.85) 85%,
      rgba(0, 190, 213, 0.87) 87%,
      #00bed5
    ),
    linear-gradient(126deg, rgba(0, 190, 213, 0), #00bed5),
    linear-gradient(#594099, #594099);
  width: 90vw;
  margin: 32px 10%;
  // mobile
  @media screen and (max-width: 425px) {
    height: 240px;
    margin: 24px 0;
    padding: 12px;
    width: 100vw;
  }
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    height: 240px;
    margin: 24px 0;
    padding: 12px;
    width: 100vw;
  }
  height: 240px;
    padding: 12px;
`;

export const DetailName = styled.p`
  // mobile
  @media screen and (max-width: 425px) {
    margin: 0;
    color: white;
    font-size: 2rem;
  }
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    margin: 0;
    color: white;
    font-size: 2rem;
  }
  margin: 0;
    color: white;
    font-size: 3rem;
`;

export const DetailDescription = styled.p`
  // mobile
  @media screen and (max-width: 425px) {
    margin: 4px;
    color: white;
    font-size: 1rem;
    width: 90%;
    text-align: justify;
    height: 144px;
    overflow-y: auto;
  }
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    margin: 4px;
    color: white;
    font-size: 1rem;
    width: 90%;
    text-align: justify;
    height: 144px;
    overflow-y: auto;
  }
  margin: 4px;
    color: white;
    font-size: 1.5rem;
    width: 90%;
    text-align: justify;
    height: 144px;
    overflow-y: auto;
`;

export const DetailChartWrapper = styled.div`
  width: 100vw;
  display: flex;
  justify-content: center;
  // mobile
  @media screen and (max-width: 425px) {
    margin: 100px 0;
    width: 100%;
  }
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    margin: 100px 0;
    width: 100%;
  }
`;
