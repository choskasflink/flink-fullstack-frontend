import styled from "styled-components";

export const HomeMainWrapper = styled.div`
  height: 100vh;
  overflow-x: hidden;
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    width: 100vw;
  }
  // Desktop
  @media screen and (min-width: 769px) {
    width: 100vw;
  }
`;

export const HomeContainerWrapper = styled.div`
  // mobile
  @media screen and (max-width: 425px) {
    overflow-y: auto;
    display: flex;
    flex-direction: column;
    height: auto;
    overflow-x: hidden;
  }
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    overflow-y: auto;
    display: flex;
    flex-direction: column;
    height: auto;
    overflow-x: hidden;
  }
`;

export const HomeTableDescriptionText = styled.p`
  // mobile
  @media screen and (max-width: 425px) {
    flex-wrap: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    width: 100px;
  }
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    flex-wrap: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    width: 100px;
  }
`;

export const NavbarContainer = styled.div`
  margin-bottom: 24px;
  width: 100vw;
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    height: 10%;
  }
  // mobile
  @media screen and (max-width: 425px) {
    height: 10%;
  }
  // Desktop
  @media screen and (min-width: 769px) {
    height: 13%;
  }
`;

export const ResponsiveTableData = styled.div`
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    display: block;
  }
  // mobile
  @media screen and (max-width: 425px) {
    display: none;
  }
  // Desktop
  @media screen and (min-width: 769px) {
    display: block;
  }
`;

export const HomeNumberOfDataText = styled.p`
  margin: 10px 24px;
  font-size: 1.3rem;
  // mobile
  @media screen and (max-width: 425px) {
    font-size: 1.1rem;
  }
`;

export const HomeWelcomeTitle = styled.p`
  text-align: center;
  margin: 10px 24px;
  font-size: 3rem;
  // mobile
  @media screen and (max-width: 425px) {
    font-size: 2rem;
    width: 90%;
  }
`;

export const HomeWelcomeText = styled.p`
  text-align: center;
  margin: 10px 24px;
  font-size: 1.3rem;
  width: 98%;
  // mobile
  @media screen and (max-width: 425px) {
    font-size: 1rem;
    width: 90%;
  }
`;

export const HomeTableContainer = styled.div`
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    display: block;
    width: 100%;
  }
  // mobile
  @media screen and (max-width: 425px) {
    display: block;
  }
  // Desktop
  @media screen and (min-width: 769px) {
    display: flex;
    width: 100%;
  }
`;

export const HomeAddCompanyButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  // mobile
  @media screen and (max-width: 425px) {
    justify-content: center;
    align-items: center;
  }
  // iPad
  @media screen and (min-width: 425px) and (max-width: 768px) {
    justify-content: flex-end;
    align-items: end;
  }
  // Desktop
  @media screen and (min-width: 769px) {
    justify-content: flex-end;
    align-items: end;
  }
`;

export const NotFoundRecordsText = styled.p`
    margin-top: 24px;
`;