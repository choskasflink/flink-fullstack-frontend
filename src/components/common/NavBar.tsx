import { Box, Flex, Spacer } from '@chakra-ui/layout';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { NavbarContainer } from '../../styles/home';

const NavBar = () => {
    const router = useRouter();
return(
    <NavbarContainer>
        <Flex alignItems="center" height="100%" width="100%" boxShadow="2xl" bg="white">
            <Box style={{cursor: 'pointer'}} onClick={() => router.push('/')} marginLeft="12px" height="25px" width="20%"><img width="150px" src='/imgs/NASDAQ.png' alt="NASDAQ"/></Box>
             <Spacer height="88px" />
            <Box style={{cursor: 'pointer'}} onClick={() => router.push('/')} marginBottom="12px" marginRight="12px" width="20%"><img width="150px" src='/imgs/NYSE.png' alt="NYSE"/></Box>
        </Flex>
    </NavbarContainer>
)};

export default NavBar;