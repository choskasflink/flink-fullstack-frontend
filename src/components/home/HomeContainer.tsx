import { Button } from "@chakra-ui/button";
import { SearchIcon } from "@chakra-ui/icons";
import { Input, InputGroup, InputLeftElement } from "@chakra-ui/input";
import React, { useEffect, useState } from "react";
import {
  HomeContainerWrapper,
  HomeNumberOfDataText,
  HomeWelcomeText,
  HomeWelcomeTitle,
  HomeAddCompanyButtonWrapper,
} from "../../styles/home";
import CompaniesTable from "./CompaniesTable";

interface HomeContainerProps {
  tableData: Array<any>;
  originalTableData: Array<any>;
  setTableData: Function;
  getTableInfo: Function;
}

const HomeContainer = ({
  tableData,
  setTableData,
  originalTableData,
  getTableInfo,
}: HomeContainerProps) => {
  const [isOpenEditModal, setIsOpenEditModal] = useState(false);
  const [isAddModal, setIsAddModal] = useState(false);
  const [searchBySymbol, setSearchBySymbol] = useState("");
  const [searchByName, setSearchByName] = useState("");
  const [isClickedSortButton, setIsClickedSortButton] = useState(false);
  const onFilterTable = (e, byName = false) => {
    const search = e.target.value.toUpperCase();
    if (search.length === 0) setTableData(originalTableData);
    const filteredResults = originalTableData.filter((item: any) => {
      if (!byName) {
        setSearchBySymbol(e.target.value);
        setSearchByName("");
        return item.symbol.toUpperCase().includes(search.toUpperCase());
      } else {
        setSearchByName(e.target.value);
        setSearchBySymbol("");
        return item.companyName.toUpperCase().includes(search.toUpperCase());
      }
    });
    setTableData(filteredResults);
  };

  const onSortCompanies = () => {
    const sorted = tableData.sort((a, b) =>
    a.companyName > b.companyName ? 1 : b.companyName > a.companyName ? -1 : 0
  );
  setTableData(sorted);
  }

  useEffect(() => {
    onSortCompanies();
  }, [isClickedSortButton]);

  return (
    <HomeContainerWrapper>
      <HomeWelcomeTitle>Stock Screener</HomeWelcomeTitle>
      <HomeWelcomeText>
        Welcome, here you can add the companies compatibles with the Nasdaq and
        Nyse market caps.
      </HomeWelcomeText>
      <InputGroup margin="12px 24px">
        <InputLeftElement
          pointerEvents="none"
          borderColor="0090ba"
          color="#0090ba"
          children={<SearchIcon color="#0090ba" />}
        />
        <Input
          onChange={(e) => onFilterTable(e)}
          value={searchBySymbol}
          width="350px"
          type="tel"
          placeholder="Search by symbol"
        />
      </InputGroup>
      <InputGroup margin="12px 24px">
        <InputLeftElement
          pointerEvents="none"
          borderColor="0090ba"
          color="#0090ba"
          children={<SearchIcon color="#0090ba" />}
        />
        <Input
          onChange={(e) => onFilterTable(e, true)}
          value={searchByName}
          width="350px"
          type="text"
          placeholder="Search by company name"
        />
      </InputGroup>
      <HomeAddCompanyButtonWrapper>
        <Button
          onClick={() => {
            setIsOpenEditModal(true);
            setIsAddModal(true);
          }}
          margin="12px 12px"
          w={[350, 240, 240, 240, 240]}
          colorScheme="teal"
          variant="outline"
        >
          Add new company
        </Button>
        <Button
          onClick={(e) => {
            e.preventDefault();
            onSortCompanies();
            setIsClickedSortButton(!isClickedSortButton);
          }}
          margin="12px 12px"
          w={[350, 240, 240, 240, 240]}
          colorScheme="teal"
          variant="outline"
        >
          Filter alphabetically by name
        </Button>
      </HomeAddCompanyButtonWrapper>
      <HomeNumberOfDataText>
        Showing: {tableData.length} / {originalTableData.length} companies
      </HomeNumberOfDataText>
      <CompaniesTable
        isAddModal={isAddModal}
        setIsAddModal={setIsAddModal}
        isOpenEditModal={isOpenEditModal}
        setIsOpenEditModal={setIsOpenEditModal}
        getTableInfo={getTableInfo}
        tableData={tableData}
        setTableData={setTableData}
      />
    </HomeContainerWrapper>
  );
};

export default HomeContainer;
