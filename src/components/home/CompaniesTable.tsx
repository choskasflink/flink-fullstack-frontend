import React, { useState } from "react";
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableCaption,
  Modal,
  ModalFooter,
  Button,
  ModalContent,
  ModalBody,
  Input,
  Text,
  ModalOverlay,
  Tfoot
} from "@chakra-ui/react";
import { Textarea } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { DeleteIcon, EditIcon, ViewIcon } from "@chakra-ui/icons";
import {
  addRegister,
  deleteRegister,
  updateRegister,
} from "../../api/apiCalls";
import { HomeTableContainer, NotFoundRecordsText } from "../../styles/home";

interface CompaniesTableProps {
  tableData: Array<any>;
  setTableData: Function;
  getTableInfo: Function;
  isOpenEditModal: boolean;
  setIsOpenEditModal: Function;
  isAddModal: boolean;
  setIsAddModal: Function;
}

const CompaniesTable = ({
  tableData,
  setTableData,
  getTableInfo,
  isOpenEditModal,
  setIsOpenEditModal,
  isAddModal,
  setIsAddModal,
}: CompaniesTableProps) => {
  const router = useRouter();
  const [arrayLimit, setArrayLimit] = useState(10);
  const [isOpenDeleteModal, setIsOpenDeleteModal] = useState(false);
  const [companyDetail, setCompanyDetail] = useState(null);
  const [companyName, setCompanyName] = useState("");
  const [companyDescription, setCompanyDescription] = useState("");
  const [companySymbol, setCompanySymbol] = useState("");

  const getTableContent = () => {
    let component;
    if (tableData.length > 0) {
      component = tableData.slice(0, arrayLimit).map((item) => (
        <Tr key={item.uuid}>
          <Td style={{width: '30%'}}> {item.companyName}</Td>
          <Td>{item.symbol}</Td>
          <Td
            style={{
              display: "flex",
              flexDirection: "row",
              height: "140px",
              justifyContent: "space-around",
              cursor: "pointer",
              alignItems: "center",
            }}
          >
            <DeleteIcon
              w={5}
              h={5}
              color="#AF0512"
              onClick={() => {
                setCompanyDetail(item);
                setIsOpenDeleteModal(true);
              }}
            />
            <EditIcon
              w={5}
              h={5}
              color="#0090ba"
              onClick={() => {
                setCompanyDetail(item);
                setIsOpenEditModal(true);
                setCompanyName(item.companyName);
                setCompanyDescription(item.companyDescription);
                setCompanySymbol(item.symbol);
              }}
            />

            <ViewIcon
              w={5}
              h={5}
              color="#0090ba"
              onClick={() => router.push(`/detail/${item.uuid}`)}
            />
          </Td>
        </Tr>
      ));
    }
    return component;
  };

  const onDelete = async (e) => {
    e.preventDefault();
    await deleteRegister(companyDetail.uuid);
    setIsOpenDeleteModal(false);
    setCompanyDetail(null);
    await getTableInfo();
  };

  const onEdit = async (e) => {
    e.preventDefault();
    const response = await updateRegister({
      companyName,
      companyDescription,
      symbol: companySymbol,
      uuid: companyDetail.uuid,
    });
    if (response.success) {
      setIsOpenEditModal(false);
      setCompanyDetail(null);
      await getTableInfo();
      setCompanyDescription("");
      setCompanyName("");
      setCompanySymbol("");
      setIsAddModal(false);
    }
  };

  const onAdd = async (e) => {
    e.preventDefault();
    const response = await addRegister({
      companyName,
      companyDescription,
      symbol: companySymbol,
    });
    if (response.success) {
      setIsOpenEditModal(false);
      setCompanyDetail(null);
      await getTableInfo();
      setCompanyDescription("");
      setCompanyName("");
      setCompanySymbol("");
      setIsAddModal(false);
    }
  };

  return (
    <>
      <HomeTableContainer>
        <Table variant="simple" width="100vw">
            {tableData.length === 0 && ( <TableCaption>No records found.</TableCaption>)}
       
          {tableData.length >= arrayLimit && (
            <TableCaption
              onClick={() => setArrayLimit(arrayLimit + 10)}
              style={{
                textDecoration: "underline",
                width: "100vw",
                fontSize: '1.3rem',
                color: "blue",
                textAlign: "center",
                cursor: 'pointer'
              }}
            >
              Ver mas
            </TableCaption>
          )}

          <Thead>
            <Tr>
              <Th>Name</Th>
              <Th>Symbol</Th>
              <Th>Actions</Th>
            </Tr>
          </Thead>
          <Tbody>{getTableContent()}</Tbody>
        </Table>
      </HomeTableContainer>
      <Modal
        isOpen={isOpenDeleteModal}
        onClose={() => setIsOpenDeleteModal(false)}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalBody>¿Deséas borrar esta compañia?</ModalBody>
          <ModalFooter>
            <Button
              marginRight="12px"
              onClick={() => setIsOpenDeleteModal(false)}
              variant="ghost"
            >
              Cancel
            </Button>
            <Button
              onClick={(e) => onDelete(e)}
              variant="solid"
              background="red"
              color="white"
            >
              Borrar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Modal isOpen={isOpenEditModal} onClose={() => setIsOpenEditModal(false)}>
        <ModalOverlay />
        <form onSubmit={(e) => (!isAddModal ? onEdit(e) : onAdd(e))}>
          <ModalContent>
            <ModalBody>
              <Text mb="8px">Company name</Text>
              <Input
                value={companyName}
                onChange={(e) => setCompanyName(e.target.value)}
                size="sm"
              />
              <Text mb="8px">Company description</Text>
              <Textarea
                value={companyDescription}
                onChange={(e) => setCompanyDescription(e.target.value)}
                size="sm"
              />
              <Text mb="8px">Company symbol</Text>
              <Input
                value={companySymbol}
                onChange={(e) => setCompanySymbol(e.target.value)}
                size="sm"
              />
              <Text marginTop="12px" mb="8px">
                * All fields are required.
              </Text>
            </ModalBody>
            <ModalFooter>
              <Button
                marginRight="12px"
                onClick={() => {
                  setIsOpenEditModal(false);
                  setCompanyDescription("");
                  setCompanyName("");
                  setCompanySymbol("");
                  setIsAddModal(false);
                }}
                variant="ghost"
              >
                Cancel
              </Button>
              <Button
                type="submit"
                variant="solid"
                background="green"
                color="white"
              >
                Confirmar
              </Button>
            </ModalFooter>
          </ModalContent>
        </form>
      </Modal>
    </>
  );
};

export default CompaniesTable;
