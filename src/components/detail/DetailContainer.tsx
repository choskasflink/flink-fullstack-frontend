import React from "react";
import {
  DetailDescription,
  DetailName,
  DetailWrapper,
} from "../../styles/detail";

interface DetailContainerProps {
  companyDetail: {
    companyDescription: string;
    companyName: string;
    createdAt: string;
    marketValues: Array<string>;
    symbol: string;
    updatedAt: string;
    uuid: string;
    _id: string;
  };
}

const DetailContainer = ({ companyDetail }: DetailContainerProps) => {
  return (
    <>
      {companyDetail && (
        <>
          <DetailWrapper>
            <DetailName>
              {companyDetail.companyName} ({companyDetail.symbol})
            </DetailName>
            <DetailDescription>
              {companyDetail.companyDescription}
            </DetailDescription>
          </DetailWrapper>
        </>
      )}
    </>
  );
};

export default DetailContainer;
