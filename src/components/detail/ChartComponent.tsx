// @ts-nocheck
import React from 'react'
import { Doughnut, Line } from 'react-chartjs-2';
import {Chart, CategoryScale, LinearScale, PointElement, LineElement, ArcElement, BarElement} from 'chart.js'
import { DetailChartWrapper } from '../../styles/detail';
Chart.register(CategoryScale);
Chart.register(LinearScale);
Chart.register(PointElement);
Chart.register(LineElement);
Chart.register(ArcElement);
Chart.register(BarElement);

interface ChartComponentProps {
  companyDetail: {
    companyDescription: string;
    companyName: string;
    createdAt: string;
    marketValues: Array<string>;
    symbol: string;
    updatedAt: string;
    uuid: string;
    _id: string;
  };
}
 
const ChartComponent = ({companyDetail}: ChartComponentProps) => {
  const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' as const,
      },
      title: {
        display: true,
        text: 'Anually Record',
      },
    },
  };
  const data = {
    labels,
    datasets: [
      {
        label: 'Dataset 1',
        data: companyDetail && companyDetail.marketValues.map(Number),
        borderColor: 'rgb(53, 162, 235)',
        backgroundColor: 'rgba(53, 162, 235, 0.5)',
      }
    ],
  };
  return (
    <DetailChartWrapper>
<Line style={{height: '100%', width: '100%'}} data={data} options={options} />
</DetailChartWrapper>
  );
}

export default ChartComponent;