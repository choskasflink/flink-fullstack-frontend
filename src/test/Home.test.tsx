/// <reference types="jest" />
import React from 'react';
import {
	render,
	screen,
	cleanup,
	waitFor,
	fireEvent,
} from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/jest-dom/extend-expect';
import HomeContainer from '../components/home/HomeContainer';
import {getAllRegistersMock} from '../__mocks__/apiCallsMock';

jest.mock('../api/apiCalls');
beforeEach(() => {
	cleanup();
});
describe('<Home />', () => {
	test('should render layout and buttons correctly', async () => {
		render(
				<HomeContainer getTableInfo={() => {}} setTableData={() => {}} originalTableData={getAllRegistersMock.data} tableData={getAllRegistersMock.data} />
		);
		await waitFor(() => {
            expect(screen.getByText('Stock Screener')).toBeInTheDocument()
			expect(screen.getByText('Add new company')).toBeInTheDocument()
            expect(screen.getByText(/Showing:/)).toBeInTheDocument()
            expect(screen.getByText(/Name/)).toBeInTheDocument()
            expect(screen.getByText(/Symbol/)).toBeInTheDocument()
            expect(screen.getByText(/Actions/)).toBeInTheDocument()
        }
		);
	});
    test('should render data on table', async () => {
		render(
				<HomeContainer getTableInfo={() => {}} setTableData={() => {}} originalTableData={getAllRegistersMock.data} tableData={getAllRegistersMock.data} />
		);
		await waitFor(() => {
            expect(screen.getByText(/AAPL/)).toBeInTheDocument()
            expect(screen.getByText(/Apple/)).toBeInTheDocument()
        }
		);
	});
});
