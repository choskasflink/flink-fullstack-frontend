/// <reference types="jest" />
import React from 'react';
import {
	render,
	screen,
	cleanup,
	waitFor,
} from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/jest-dom/extend-expect';
import DetailContainer from '../components/detail/DetailContainer';
import {companyDetailMock, getAllRegistersMock} from '../__mocks__/apiCallsMock';

jest.mock('../api/apiCalls');
beforeEach(() => {
	cleanup();
});
describe('<Detail />', () => {
	test('should render description correctly', async () => {
		render(
				<DetailContainer companyDetail={companyDetailMock}  />
		);
		await waitFor(() => {
            expect(screen.getByText('Apple Inc. is an American multinational technology company that specializes in consumer electronics.')).toBeInTheDocument()
            expect(screen.getAllByText(/Apple/)[0]).toBeInTheDocument()
        }
		);
	});
});
