import React, { useEffect, useState } from 'react';
import { getAllRegisters } from '../api/apiCalls';
import NavBar from '../components/common/NavBar';
import HomeContainer from '../components/home/HomeContainer';
import { HomeMainWrapper } from '../styles/home';

const Index = () => {
    const [tableData, setTableData] = useState([]);
    const [originalTableData, setOriginalTableData] = useState([]);
    const getTableInfo = async () => {
        const data = await getAllRegisters();
        setTableData(data);
        setOriginalTableData(data);
    }
useEffect(() => {
    getTableInfo()
},[])
return (
    <HomeMainWrapper>
    <NavBar />
    <HomeContainer getTableInfo={getTableInfo} originalTableData={originalTableData} tableData={tableData} setTableData={setTableData} />
    </HomeMainWrapper>
)}

export default Index;