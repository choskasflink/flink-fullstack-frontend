import { ArrowLeftIcon } from '@chakra-ui/icons';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { getRegisterById } from '../../api/apiCalls';
import NavBar from '../../components/common/NavBar';
import ChartComponent from '../../components/detail/ChartComponent';
import DetailContainer from '../../components/detail/DetailContainer';

const DetailCompany = () => {
    const router = useRouter();
    const [companyDetail, setCompanyDetail] = useState(null);
    const { uuid } = router.query;

    const getCompanyDetails = async () => {
        if (uuid){
        const data = await getRegisterById(uuid)
        setCompanyDetail(data);
        }
    }
    useEffect(() => {
        getCompanyDetails();
    }, [uuid])
return (
    <>
    <NavBar />
    <ArrowLeftIcon style={{cursor: 'pointer'}} width="64px" color="rgba(53, 162, 235, 0.5)" onClick={() => router.push('/')} />
    <DetailContainer companyDetail={companyDetail} />
    <ChartComponent companyDetail={companyDetail} />
    </>
)};

export default DetailCompany;