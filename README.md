# Flink Fullstack dron Test
## Installation
```
1. Clone Repo
2. Do "yarn install"
3. Run with "yarn run dev"
4. Test with "yarn run test"
```
### Content
- Live frontend is [here](http://fullstack-frontend.s3-website-us-west-2.amazonaws.com/)
- Mobile first (Recommended iPhone 12 pro, Ipad Mini, Desktop)
- Complete CRUD frontend app
- Jest and react testing library  testing
- Gitlab CI/CD to AWS S3
- Dockerized