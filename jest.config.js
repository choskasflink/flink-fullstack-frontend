module.exports = {
	preset: 'ts-jest',
	testEnvironment: 'jsdom',
	globals: {
		'ts-jest': {
			tsConfig: 'tsconfig.jest.json',
		},
	},
	setupFilesAfterEnv: ['<rootDir>/src/setupTests.ts'],
	setupFiles: ["jest-canvas-mock"],
	moduleNameMapper: {
		'^@components(.*)$': '<rootDir>/src/components$1',
		'^@pages(.*)$': '<rootDir>/src/pages$1',
		'^@styles(.*)$': '<rootDir>/src/styles$1',
		'^@constants(.*)$': '<rootDir>/src/constants$1',
		'^@helpers(.*)$': '<rootDir>/src/lib$1',
	},
};
